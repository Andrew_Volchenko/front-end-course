
isOnline = false;

window.onload = function(){



    NewTube = {

        cliplist: [],
        clipCount: 0,
        xhttp: new XMLHttpRequest(),
        body: document.getElementsByTagName("body")[0],
        clientXDiff: 0,
        currentSlide: 1,
        lastShownSlide: 1,
        slideCount: 1,
        slideCapacity: 1,
        lastShownVideo: 0,
        stylesheet: "",

        drawDefaultElements: function(){

            var header = document.createElement("header");
            header.innerHTML = "" +
                "<a href=\"#\"><img src=\"i/logo.png\" alt=\"\"/></a>" +
                "<div class=\"search\"><input type=\"search\" class=\"search-input\" placeholder=\"Search\"/><button class=\"search-submit\">&#xf002</button></div>"

            this.slider = document.createElement("div");
            this.slider.className = "slider";
            this.slider.style.webkitTransform = "translate(0)"

            this.loadingIndicator = document.createElement("div");
            this.loadingIndicator.className = "loading-indicator";

            this.error = document.createElement("div");
            this.error.className = "error";
            this.error.appendChild(document.createElement("p"));

            this.footer = document.createElement("footer");
            var leftArrow = document.createElement("span");
            leftArrow.className = "left-arrow";

            var rightArrow = document.createElement("span");
            rightArrow.className = "right-arrow";

            this.paginator = document.createElement("div");

            this.paginator.className = "paginator";

            this.footer.appendChild(leftArrow);
            this.footer.appendChild(rightArrow);
            this.footer.appendChild(this.paginator);
            this.body.appendChild(header);
            this.body.appendChild(this.slider);
            this.body.appendChild(this.footer);
            this.body.appendChild(this.loadingIndicator);
            this.body.appendChild(this.error);

            this.slides = document.getElementsByClassName("slide");

            this.stylesheet = document.createElement("style");
            document.getElementsByTagName("script")[0].parentNode.appendChild(this.stylesheet);
            this.stylesheet = this.stylesheet.sheet || this.stylesheet.styleSheet;
        },

        bindSearchHandlers: function(){

            var search = document.getElementsByClassName("search-input")[0];
            var searchSubmit = document.getElementsByClassName("search-submit")[0];

            search.addEventListener("keypress", function(e){
                if(e.keyCode == 13){
                    this.blur();
                    if(search.value){
                        NewTube.slideTo(1);
                        NewTube.search(search.value);
                    }
                }
            });

            searchSubmit.addEventListener("click", function(){
                if(search.value){
                    NewTube.slideTo(1);
                    NewTube.search(search.value);
                }
            });

            this.xhttp.addEventListener("readystatechange", function(){
                if(this.readyState == 4){
                    if(this.status == 200){
                        NewTube.convertYouTubeResponse(JSON.parse(this.responseText));
                        if(NewTube.cliplist.length == 0)
                            NewTube.showError("No results on your request")
                        else
                            NewTube.hideError();
                            NewTube.vizualizeCliplist();
                    }
                }
            });

        },

        search: function(searchString){
            NewTube.cliplist.length = 0;
            var str = "http://gdata.youtube.com/feeds/api/videos/?v=2&alt=json&max-results=50&start-index=1&q=" + searchString;
            this.xhttp.open("get", str, true);
            this.xhttp.send();
            this.showLoading();
        },

        showError: function(errorMessage){
            this.error.getElementsByTagName("p")[0].innerHTML = errorMessage;
            this.error.className = "error visible";
        },

        hideError: function(){
            this.error.className = "error"
        },

        convertYouTubeResponse: function(rawYouTubeData) {
            var entry = {};
            var entries = rawYouTubeData.feed.entry;
            if (entries) {
                for (var i = 0; i < entries.length; i++){
                    entry = entries[i];

                    var date = new Date(Date.parse(entry.updated.$t));
                    var shortId = entry.id.$t.match(/video:.*/).toString().split(":")[1];
                    var counts = 0;
                    if("yt$statistics" in entry)
                        counts = entry.yt$statistics.viewCount;
                    var description = entry.media$group.media$description.$t;
                    if(description.length > 90)
                        description = description.slice(0, 87).concat("...");
                    this.cliplist.push({
                        id: shortId,
                        youtubeLink: "http://www.youtube.com/watch?v=" + shortId,
                        title: entry.title.$t,
                        thumbnail: entry.media$group.media$thumbnail[1].url,
                        description: description,
                        author: entry.author[0].name.$t,
                        publishDate: date.toUTCString(),
                        viewCount: counts
                    });
                }
            }
        },


        showLoading: function(){
            this.loadingIndicator.className = "loading-indicator visible";
        },

        hideLoading: function(){
            var timeout = setTimeout(function(){
                NewTube.loadingIndicator.className = "loading-indicator";
                clearTimeout(timeout);
            }, 1000);
        },

        getSlideCapacity : function(){
            NewTube.linesount = 1;
            if(NewTube.body.offsetHeight >= 835)
                NewTube.linesount = 2;
            NewTube.colsCount = 1;
            if(NewTube.body.offsetWidth <= 500){
                NewTube.colsCount = 1;
            } else if(NewTube.body.offsetWidth > 500 && NewTube.body.offsetWidth < 760){
                NewTube.colsCount = 2;
            } else if(NewTube.body.offsetWidth >= 760 && NewTube.body.offsetWidth < 1010){
                NewTube.colsCount = 3;
            } else if(NewTube.body.offsetWidth >= 1010 && NewTube.body.offsetWidth < 1300){
                NewTube.colsCount = 4;
            } else NewTube.colsCount = 5;
            NewTube.slideCapacity = NewTube.linesount * NewTube.colsCount;
        },

        vizualizeCliplist: function(){

            NewTube.showLoading();

            NewTube.slider.style.webkitTransform = "translate(0)";
            NewTube.currentSlide = 1;
            NewTube.getSlideCapacity();
            NewTube.clientXDiff = 0;
            NewTube.paginator.innerHTML = "";
            this.slider.innerHTML = "";
            this.cliplistCount = this.cliplist.length;
            var video;
            var slide;
            for(var i = 0; i < this.cliplistCount; i++){
                if(!this.cliplist[i].description)
                    this.cliplist[i].description = "Author didn't said anything about his video";
                video = document.createElement("div");
                video.className = "video";
                video.innerHTML = "" +
                    "<a href='" + this.cliplist[i].youtubeLink + "' class='video-title' draggable='false'>" + this.cliplist[i].title +"</a>" +
                    "<img src='" + this.cliplist[i].thumbnail +"' alt='' draggable='false'>" +
                    "<div class='author'>" + this.cliplist[i].author + "</div>" +
                    "<div class='two-column'>" +
                    "<div class='column views'>" + this.cliplist[i].viewCount + "</div>" +
                    "<div class='column publish-date'>" + this.cliplist[i].publishDate.slice(5, 16) + "</div>" +
                    "</div>" +
                    "<p>" + this.cliplist[i].description +"</p>" +
                    "<div class='bl'>" +
                    "<a href='" + this.cliplist[i].youtubeLink + "' draggable='false' target='_blank'>More Info</a>" +
                    "</div>";

                if(i % NewTube.slideCapacity == 0){
                    slide = document.createElement("div");
                    slide.className = "slide";
                    slide.appendChild(video);
                    this.slider.appendChild(slide);
                } else{
                    slide.appendChild(video);
                }
            }

            var sliderHeight = "height:" + 380 * NewTube.linesount + "px";

            this.addRule(".slider", sliderHeight);

            NewTube.slideCount = NewTube.slides.length + 1;

            if(NewTube.body.offsetWidth < NewTube.slideCount * 30){
                NewTube.paginator.className = "paginator hidden";
            } else NewTube.paginator.className = "paginator";

            var paginatorInner = "</span><a href='#' data-page=\"1\" class=\"active\"></a>"


            for(var i = 2; i < NewTube.slideCount; i++){
                paginatorInner = paginatorInner.concat("<a href='#' data-page=\"" + i +"\"></a>");
            }

            NewTube.paginator.innerHTML = paginatorInner;

            document.getElementsByTagName("footer")[0].appendChild(NewTube.paginator);

            var sliderWidth = "width:" + NewTube.slideCount * 100 + "%";

            this.addRule(".slider", sliderWidth);

            var slideWidth = "width:" + (100 / NewTube.slideCount) + "%";
            var videoWidth = "width:" + (100 / NewTube.colsCount - 1.25) + "%";

            this.addRule(".slide", slideWidth);
            this.addRule(".video", videoWidth);

            if(NewTube.lastShownVideo){
                NewTube.slideTo(Math.floor(NewTube.lastShownVideo / NewTube.slideCapacity) + 1);
            }

            NewTube.slideCount--;

            this.hideLoading();
        },

        addRule: function(selector, cssText) {

            var rules = NewTube.stylesheet.cssRules || NewTube.stylesheet.rules;
            var index = rules.length;

            if (NewTube.stylesheet.insertRule) {
                NewTube.stylesheet.insertRule(selector + " " + "{" + cssText + "}", index);
            } else {
                NewTube.stylesheet.addRule(selector, cssText, rules.length);
            }

            return rules[index];
        },



        bindUIHandlers: function(){
            var mousedown = false;
            var mousedownX = 0;
            var translate = 0;
            var clientX = 0;

            this.slider.addEventListener("mousedown", function(e){
                mousedown = true;
                mousedownX = e.clientX - parseInt(this.style.webkitTransform.slice(10, this.style.webkitTransform.length));
                clientX = e.clientX;
                NewTube.clientXDiff = 0;

            });

            this.slider.addEventListener("mousemove", function(e){
                if(mousedown){
                    NewTube.clientXDiff = clientX - e.clientX;
                    translate = e.clientX - mousedownX;
                    this.style.webkitTransform = "translate(" + translate + "px)";
                    if(NewTube.clientXDiff > 75){
                        NewTube.slide("left")
                        mousedown = false;
                    } else if(NewTube.clientXDiff < -75){
                        NewTube.slide("right")
                        mousedown = false;
                    }
                }

            });

            this.footer.addEventListener("click", function(e){
                switch (e.target.className){
                    case "left-arrow" : {
                        NewTube.slide("right");
                        break;
                    }

                    case "right-arrow" : {
                        NewTube.slide("left");
                        break;
                    }

                }
            });

            this.slider.addEventListener("transitionend", function(){
                this.className = "slider";
                mousedown = false;
                NewTube.lastShownVideo = (NewTube.currentSlide - 1) * NewTube.slideCapacity;
            });

            this.slider.addEventListener("mouseup", function(){
                mousedown = false;
                if((NewTube.clientXDiff <= 75 && NewTube.clientXDiff >= 0) || (NewTube.clientXDiff >= -75 && NewTube.clientXDiff <= 0))
                    NewTube.slide("normal")
                else
                    NewTube.clientXDiff = 0;
            });

            this.paginator.addEventListener("click", function(e){
                if(e.target.dataset.page != undefined){
                    NewTube.slideTo(e.target.dataset.page);
                } else if(e.target.className == "left-arrow"){
                    NewTube.slide("left");
                } else if(e.target.className == "right-arrow")
                    NewTube.slide("right");
            });
        },

        slide: function(direction){
            NewTube.slider.className = "slider sliding";
            var x;
            var translate = NewTube.slider.style.webkitTransform;
            translate = parseInt(translate.slice(10, translate.length));
            switch (direction){
                case "left": {
                    if(NewTube.currentSlide < NewTube.slideCount){
                        x = translate + NewTube.clientXDiff - NewTube.body.offsetWidth;
                        NewTube.paginator.childNodes[NewTube.currentSlide].className = "active";
                        NewTube.paginator.childNodes[NewTube.currentSlide - 1].className = "";
                        NewTube.currentSlide++;

                    } else {
                        NewTube.slide("normal");
                    }
                    break;
                }
                case "right": {
                    if(NewTube.currentSlide > 1){
                        x = translate + NewTube.clientXDiff + NewTube.body.offsetWidth;
                        NewTube.currentSlide--;
                        NewTube.paginator.childNodes[NewTube.currentSlide].className = "";
                        NewTube.paginator.childNodes[NewTube.currentSlide - 1].className = "active";
                    } else {
                        NewTube.slide("normal");
                    }
                    break;
                }
                case "normal": {
                    x = translate + NewTube.clientXDiff;
                    NewTube.clientXDiff = 0;
                }
            }
            this.slider.style.webkitTransform = "translate(" + x + "px)";
        },

        slideTo: function(slideNumber){
            while(NewTube.currentSlide != slideNumber){
                if(NewTube.currentSlide < slideNumber){
                    NewTube.slide("left");
                }
                else {
                    NewTube.slide("right");
                }
            }
        }
    };

    try{
        NewTube.drawDefaultElements();
        if(isOnline){
            NewTube.bindSearchHandlers();
            NewTube.bindUIHandlers();
        } else NewTube.showError("Check your internet connection please and try again")
    } catch(error){
        NewTube.showError(error.message);
    }


}

window.addEventListener("resize", function(e){
    NewTube.vizualizeCliplist();
});



