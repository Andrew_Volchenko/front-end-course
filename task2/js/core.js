/**
 * Created by Andrew Volchenko on 25.11.13.
 */

    var mouse;

    window.onload = function(){

        var bodyNode = document.getElementsByClassName("body")
        var body = bodyNode.item(0)
        var ul = document.createElement("ul")
        body.appendChild(ul)
        ul = document.getElementsByTagName("ul")
        ul = ul.item(0)
        ul.innerHTML = "<li id='backSlide'></li><li id='topSlide'></li>"

        var liNode = document.getElementsByTagName("li")
        console.dir(liNode)


        var back = document.getElementById("backSlide")

        var mousePositions = [];
        var topSlide = document.getElementById("topSlide")
        var scale = 0.5

        function animation(animationName, duration, endTranslate, _this){
            _this.style.webkitAnimation = animationName + " " + duration / 1000 + "s"
            var slideTimeout = setTimeout(function(self){
                self.style.webkitTransform = "translate(" + endTranslate + "px)"
                self.style.webkitAnimation = "emptyAnimation 0s"
                clearTimeout(Timeout)
            }, duration, _this);
        }

        function backAnimation(animationName, duration, scale){
            back.style.webkitAnimation = animationName + " " + duration / 1000 + "s"
            var scaleTimeout = setTimeout(function(){
                back.style.webkitTransform = "scale(" + scale + ")"
                back.style.webkitAnimation = "emptyAnimation 0s"
                clearTimeout(scaleTimeout)
            }, duration);
        }

        Array.prototype.slice.call(liNode).map(function(item, i){

            item.mouse = {
                down: false,
                startX: 0
            };

            item.style.webkitTransform = "translate(0px)"
            back.style.webkitTransform = "translate(0px) scale(0.5)"

            item.onmousedown = function(e){
                this.mouse.down = true
                this.mouse.startX = e.clientX - parseInt(this.style.webkitTransform.slice(10, this.style.webkitTransform.length))
            }

            item.onmouseup = function(){
                this.mouse.down = false
                var translate = parseInt(this.style.webkitTransform.slice(10, this.style.webkitTransform.length))
                if(translate < -75 && translate < 0){
                    animation("slideLeft", 1000, -1280, this)
                    backAnimation("scaleToNormal", 1000, 1)
                }
                if(translate >= -75 && translate < 0){
                    animation("slideToNormal", 500, 0, this)
                    backAnimation("scaleToMin", 500, 0.5)
                }

                if(translate > 75){
                    animation("slideRight", 1000, 1280, this)
                    backAnimation("scaleToNormal", 1000, 1)
                }
                if(translate <= 75 && translate > 0){
                    animation("slideToNormal", 500, 0, this)
                    backAnimation("scaleToMin", 500, 0.5)
                }

                console.log(translate)
            }

            item.onmousemove = function(e){
                if(this.mouse.down){
                    x = e.clientX - this.mouse.startX
                    this.style.webkitTransform = "translate(" + x + "px)"
                    if(scale < 1){
                        scale = Math.abs(x) / screen.width + 0.5
                        console.log(scale)
                        back.style.webkitTransform = "translate(0px) scale(" + scale + ")"
                    }
                }

            }

            //------------------------ TOUCH EVENTS ----------------------------------

            item.addEventListener('touchstart', function(ev) {
                mouse.down = true;
            }, false);

            item.addEventListener('touchmove', function (ev) {
                if(this.mouse.down){
                    x = e.clientX - this.mouse.startX
                    this.style.webkitTransform = "translate(" + x + "px)"
                    if(scale < 1){
                        scale = Math.abs(x) / screen.width + 0.5
                        console.log(scale)
                        back.style.webkitTransform = "translate(0px) scale(" + scale + ")"
                    }
                }
            })

            item.addEventListener('touchend', function(ev){
                this.mouse.down = false
                var translate = parseInt(this.style.webkitTransform.slice(10, this.style.webkitTransform.length))
                if(translate < -75 && translate < 0){
                    animation("slideLeft", 1000, -1280, this)
                    backAnimation("scaleToNormal", 1000, 1)
                }
                if(translate >= -75 && translate < 0){
                    animation("slideToNormal", 500, 0, this)
                    backAnimation("scaleToMin", 500, 0.5)
                }

                if(translate > 75){
                    animation("slideRight", 1000, 1280, this)
                    backAnimation("scaleToNormal", 1000, 1)
                }
                if(translate <= 75 && translate > 0){
                    animation("slideToNormal", 500, 0, this)
                    backAnimation("scaleToMin", 500, 0.5)
                }

            })

        })


    }